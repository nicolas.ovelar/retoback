package org.sofka.demo.repository;

import org.sofka.demo.models.Cyclist;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CyclistRepository extends ReactiveMongoRepository<Cyclist, String> {
	Mono<Cyclist> findCyclistByCompetitorNumber(String competitorNumber);
}
