package org.sofka.demo.repository;

import org.sofka.demo.models.CyclistTeam;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CyclistTeamRepository extends ReactiveMongoRepository<CyclistTeam, String> {
	Mono<CyclistTeam> findCyclistTeamByTeamCode(String teamCode);
	Mono<CyclistTeam> findCyclistTeamByCountryCode(String countryCode);
}
