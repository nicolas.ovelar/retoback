package org.sofka.demo.controller;

import org.sofka.demo.dto.CyclistDTO;
import org.sofka.demo.dto.CyclistTeamDTO;
import org.sofka.demo.services.CyclistTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/team")
public class CyclistTeamController {

    @Autowired
    private CyclistTeamService service;

    @PostMapping("/createTeam")
    public ResponseEntity<Mono<CyclistTeamDTO>> save(@RequestBody CyclistTeamDTO cyclistTeamDTO) {
        return ResponseEntity.ok()
                .body(service
                        .saveCyclistTeam(cyclistTeamDTO));
    }

    @PostMapping("/addCyclist/{idTeam}/{idCyclist}")
    public ResponseEntity<Mono<CyclistTeamDTO>> addCyclist(@PathVariable("idTeam") String idTeam,
                                                           @PathVariable("idCyclist") String idCyclist) {
        return ResponseEntity.ok()
                .body(service
                        .addCyclistToTeam(idTeam,idCyclist));
    }

    @GetMapping("/findAll")
    public ResponseEntity<Flux<CyclistTeamDTO>> findAll() {
        return ResponseEntity.ok()
                .body(service
                        .findAllCyclistTeams());
    }

    @GetMapping("/findAllCyclist/{teamCode}")
    public ResponseEntity<Flux<CyclistDTO>> allCyclistByTeamCode(@PathVariable("teamCode") String teamCode) {
        return ResponseEntity.ok()
                .body(service
                        .findAllCyclistByTeamCode(teamCode));
    }

    @GetMapping("/findTeamByTeamCode/{teamCode}")
    public ResponseEntity<Mono<CyclistTeamDTO>> teamByCode(@PathVariable("teamCode") String teamCode) {
        return ResponseEntity.ok()
                .body(service
                        .findCyclistTeamByTeamCode(teamCode));
    }

    @GetMapping("/findTeamByCountryCode/{countryCode}")
    public ResponseEntity<Mono<CyclistTeamDTO>> teamByCountry(@PathVariable("countryCode") String countryCode) {
        return ResponseEntity.ok()
                .body(service
                        .findCyclistTeamByCountryCode(countryCode));
    }

    @PutMapping("update/{id}")
    public ResponseEntity<Mono<CyclistTeamDTO>> updateTeam(@RequestBody CyclistTeamDTO cyclistTeamDTO,
                                                           @PathVariable("id") String id) {
        return ResponseEntity.ok()
                .body(service
                        .update(cyclistTeamDTO,id));
    }

    @DeleteMapping("delete/{id}")
    public Mono<Void> deleteById(@PathVariable("id") String id) {
        return service.delete(id);
    }

}
