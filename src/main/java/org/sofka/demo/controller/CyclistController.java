package org.sofka.demo.controller;

import org.sofka.demo.dto.CyclistDTO;
import org.sofka.demo.services.CyclistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/cyclist")
public class CyclistController {

    @Autowired
    private CyclistService service;

    @PostMapping("/create")
    public ResponseEntity<Mono<CyclistDTO>> saveCyclist(@RequestBody CyclistDTO cyclistDTO) {
        return ResponseEntity.ok()
                .body(service
                        .save(cyclistDTO));
    }

    @GetMapping("/findAll")
    public ResponseEntity<Flux<CyclistDTO>> findCyclist() {
        return ResponseEntity.ok()
                .body(service
                        .findAllCyclists());
    }

    @GetMapping("/findByCompetitorNumber/{competitorNumber}")
    public ResponseEntity<Mono<CyclistDTO>> findByCompetitorNumber(@PathVariable(value = "competitorNumber") String competitorNumber) {
        return ResponseEntity.ok()
                .body(service
                        .findCyclistByCompetitorNumber(competitorNumber));
    }

    @GetMapping("/findByCountry/{countryCode}")
    public ResponseEntity<Flux<CyclistDTO>> findByCountry(@PathVariable(value = "countryCode") String countryCode) {
        return ResponseEntity.ok()
                .body(service
                        .findAllCyclistsByCountry(countryCode));
    }

    @PutMapping("update/{id}")
    public ResponseEntity<Mono<CyclistDTO>> updateCyclist(@RequestBody CyclistDTO cyclistDTO,
                                                          @PathVariable(value = "id") String id) {
        return ResponseEntity.ok()
                .body(service
                        .update(cyclistDTO,id));
    }

    @DeleteMapping("delete/{id}")
    public Mono<Void> deleteById(@PathVariable("id") String id) {
        return service.delete(id);
    }

}
