package org.sofka.demo.controller;

import org.sofka.demo.dto.CountryDTO;
import org.sofka.demo.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/country")
public class CountryController {

    @Autowired
    private CountryService service;

    @PostMapping("/create")
    public ResponseEntity<Mono<CountryDTO>> save(@RequestBody CountryDTO countryDTO) {
        return ResponseEntity.ok()
                .body(service
                        .save(countryDTO));
    }

    @GetMapping("/findAll")
    public ResponseEntity<Flux<CountryDTO>> findAll() {
        return ResponseEntity.ok()
                .body(service
                        .findAllCountry());
    }

    @GetMapping("/findByCode/{code}")
    public ResponseEntity<Mono<CountryDTO>> countryByCode(@PathVariable(value = "code") String code) {
        return ResponseEntity.ok()
                .body(service
                        .findCountryByCode(code));
    }

    @PutMapping("update/{id}")
    public ResponseEntity<Mono<CountryDTO>> updateCountry(@RequestBody CountryDTO countryDTO,
                                                          @PathVariable(value = "id") String id) {
        return ResponseEntity.ok()
                .body(service
                        .update(countryDTO,id));
    }

    @DeleteMapping("delete/{id}")
    public Mono<Void> deleteById(@PathVariable("id") String id) {
        return service.delete(id);
    }

}
