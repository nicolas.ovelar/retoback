package org.sofka.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.sofka.demo.models.Cyclist;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CyclistTeamDTO {

    private String id;
    private String name;
    private String teamCode;
    private String countryCode;
    private List<Cyclist> cyclists;
}
