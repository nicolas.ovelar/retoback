package org.sofka.demo.services;

import org.sofka.demo.dto.CountryDTO;
import org.sofka.demo.interfaces.ICountryService;
import org.sofka.demo.models.Country;
import org.sofka.demo.repository.CountryRepository;
import org.sofka.demo.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CountryService implements ICountryService {

    @Autowired
    private CountryRepository repository;

    @Override
    public Mono<CountryDTO> save(CountryDTO countryDTO) {
        if(!repository.findCountryByCode(countryDTO.getCode()).equals(countryDTO.getCode())) {
            Country country = AppUtils.dtoToCountry(countryDTO);
            return repository.save(country)
                    .map(AppUtils::countryToDto);
        } else {
            return null;
        }
    }

    @Override
    public Flux<CountryDTO> findAllCountry() {
        return repository.findAll()
                .map(AppUtils::countryToDto);
    }

    @Override
    public Mono<CountryDTO> findCountryByCode(String code) {
        return repository.findCountryByCode(code)
                .map(AppUtils::countryToDto);
    }

    @Override
    public Mono<CountryDTO> update(CountryDTO countryDTO, String id) {
        return repository.findById(id)
                .flatMap(country -> {
                    Country country1 = AppUtils.dtoToCountry(countryDTO);
                    country1.setId(id);
                    return repository.save(country1);
                })
                .map(AppUtils::countryToDto);
    }

    @Override
    public Mono<Void> delete(String id) {
        return repository.deleteById(id);
    }
}
