package org.sofka.demo.services;

import org.sofka.demo.dto.CyclistDTO;
import org.sofka.demo.dto.CyclistTeamDTO;
import org.sofka.demo.interfaces.ICyclistTeamService;
import org.sofka.demo.models.Cyclist;
import org.sofka.demo.models.CyclistTeam;
import org.sofka.demo.repository.CyclistRepository;
import org.sofka.demo.repository.CyclistTeamRepository;
import org.sofka.demo.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

@Service
public class CyclistTeamService implements ICyclistTeamService {

    @Autowired
    private CyclistTeamRepository repository;

    @Autowired
    private CyclistRepository cyclistRepository;

    @Override
    public Mono<CyclistTeamDTO> addCyclistToTeam(String idTeam, String idCyclist) {
        return repository.findById(idTeam)
                .flatMap(cyclistTeam -> {
                        if(cyclistTeam.getCyclists().size() < 8) {
                            return cyclistRepository.findById(idCyclist)
                                    .flatMap(cyclist -> {
                                        if (!cyclistTeam.getCyclists().contains(cyclist)) {
                                            cyclistTeam.getCyclists().add(cyclist);
                                            return repository.save(cyclistTeam);
                                        }
                                        return Mono.just(cyclistTeam);
                                    });
                    }
                    return Mono.just(cyclistTeam);
                }).map(AppUtils::cyclistTeamToDto);
    }

    @Override
    public Mono<CyclistTeamDTO> saveCyclistTeam(CyclistTeamDTO cyclistTeamDTO) {
        CyclistTeam cyclistTeam = AppUtils.dtoToCyclistTeam(cyclistTeamDTO);
        return repository.save(cyclistTeam)
                .map(AppUtils::cyclistTeamToDto);
    }
    @Override
    public Flux<CyclistTeamDTO> findAllCyclistTeams() {
        return repository.findAll()
                .map(AppUtils::cyclistTeamToDto);
    }

    @Override
    public Flux<CyclistDTO> findAllCyclistByTeamCode(String teamCode) {
        return repository.findCyclistTeamByTeamCode(teamCode)
                .flatMapIterable(CyclistTeam::getCyclists)
                .map(AppUtils::cyclistToDto);
    }

    @Override
    public Mono<CyclistTeamDTO> findCyclistTeamByTeamCode(String teamCode) {
        return repository.findCyclistTeamByTeamCode(teamCode)
                .map(AppUtils::cyclistTeamToDto);
    }

    @Override
    public Mono<CyclistTeamDTO> findCyclistTeamByCountryCode(String countryCode) {
        return repository.findCyclistTeamByCountryCode(countryCode)
                .map(AppUtils::cyclistTeamToDto);
    }

    @Override
    public Mono<CyclistTeamDTO> update(CyclistTeamDTO cyclistTeamDTO, String id) {
        return repository.findById(id)
                .flatMap(cyclistTeam -> {
                    CyclistTeam cyclistTeam1 = AppUtils.dtoToCyclistTeam(cyclistTeamDTO);
                    cyclistTeam1.setId(id);
                    return repository.save(cyclistTeam1);
                })
                .map(AppUtils::cyclistTeamToDto);
    }

    @Override
    public Mono<Void> delete(String id) {
        return repository.deleteById(id);
    }


}
