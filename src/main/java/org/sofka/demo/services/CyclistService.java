package org.sofka.demo.services;

import org.sofka.demo.dto.CyclistDTO;
import org.sofka.demo.interfaces.ICyclistService;
import org.sofka.demo.models.Cyclist;
import org.sofka.demo.repository.CyclistRepository;
import org.sofka.demo.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CyclistService implements ICyclistService {

    @Autowired
    private CyclistRepository repository;

    @Override
    public Mono<CyclistDTO> save(CyclistDTO cyclistDTO) {
        Cyclist cyclist = AppUtils.dtoToCyclist(cyclistDTO);
        return repository.save(cyclist)
                .map(AppUtils::cyclistToDto);
    }

    @Override
    public Flux<CyclistDTO> findAllCyclists() {
        return repository.findAll()
                .map(AppUtils::cyclistToDto);
    }

    @Override
    public Mono<CyclistDTO> findCyclistByCompetitorNumber(String competitorNumber) {
        return repository.findCyclistByCompetitorNumber(competitorNumber)
                .map(AppUtils::cyclistToDto);
    }

    @Override
    public Flux<CyclistDTO> findAllCyclistsByCountry(String countryCode) {
        return repository.findAll().filter(cyclist -> cyclist.getCountryCode().equals(countryCode))
                .map(AppUtils::cyclistToDto);
    }

    @Override
    public Mono<CyclistDTO> update(CyclistDTO cyclistDTO, String id) {
        return repository.findById(id)
                .flatMap(cyclist -> {
                    Cyclist cyclist1 = AppUtils.dtoToCyclist(cyclistDTO);
                    cyclist1.setId(id);
                    return repository.save(cyclist1);
                })
                .map(AppUtils::cyclistToDto);
    }

    @Override
    public Mono<Void> delete(String id) {
        return repository.deleteById(id);
    }
}
