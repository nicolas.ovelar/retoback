package org.sofka.demo.interfaces;

import org.sofka.demo.dto.CyclistDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICyclistService {
    Mono<CyclistDTO> save(CyclistDTO cyclistDTO);
    Flux<CyclistDTO> findAllCyclists();
    Mono<CyclistDTO> findCyclistByCompetitorNumber(String competitorNumber);
    Flux<CyclistDTO> findAllCyclistsByCountry(String country);
    Mono<CyclistDTO> update(CyclistDTO cyclistDTO, String id);
    Mono<Void> delete(String id);

}
