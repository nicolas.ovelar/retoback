package org.sofka.demo.interfaces;

import org.sofka.demo.dto.CyclistDTO;
import org.sofka.demo.dto.CyclistTeamDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICyclistTeamService {
    Mono<CyclistTeamDTO> addCyclistToTeam(String idTeam, String idCyclist);
    Mono<CyclistTeamDTO> saveCyclistTeam(CyclistTeamDTO cyclistTeamDTO);
    Flux<CyclistTeamDTO> findAllCyclistTeams();
    Flux<CyclistDTO> findAllCyclistByTeamCode(String teamCode);
    Mono<CyclistTeamDTO> findCyclistTeamByTeamCode(String teamCode);
    Mono<CyclistTeamDTO> findCyclistTeamByCountryCode(String countryCode);
    Mono<CyclistTeamDTO> update(CyclistTeamDTO cyclistTeamDTO, String id);
    Mono<Void> delete(String id);




}
