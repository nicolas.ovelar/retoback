package org.sofka.demo.interfaces;

import org.sofka.demo.dto.CountryDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICountryService {
    Mono<CountryDTO> save(CountryDTO countryDTO);
    Flux<CountryDTO> findAllCountry();
    Mono<CountryDTO> findCountryByCode(String code);
    Mono<CountryDTO> update(CountryDTO countryDTO, String id);
    Mono<Void> delete(String id);

}
